#!/usr/bin/env python3

# Script that replaces UUIDs in the input file to the UUID4 random ones.

import os
import sys
import re 
import uuid

def main():

  if (len(sys.argv) > 1):
    filename = sys.argv[1]
  else:
    print('no file to patch')

  if (os.path.isfile(filename)):
    with open(filename, 'r') as f:
      data = f.read()
      pattern = re.compile("[0-F]{8}-([0-F]{4}-){3}[0-F]{12}", re.I)
      uuids = set()
      pos = 0
      while (mo := pattern.search(data, pos)):
        if (not mo.group() in uuids):
          random_uuid  = str(uuid.uuid4())
          uuids.add(random_uuid)
          data = data.replace(mo.group(), random_uuid)
        pos = mo.end()
    with open(filename + '.fixed', 'w') as f:
      f.write(data) 

if __name__ == "__main__":
    main()
